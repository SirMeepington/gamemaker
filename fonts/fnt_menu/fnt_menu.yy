{
    "id": "3c6b6e34-d8c0-450c-994c-a39d3b9b0dc2",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Franklin Gothic Demi",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dbf2a89f-b16f-44f3-a234-ceb55a4b7bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 163,
                "y": 162
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3deb9b56-6c2c-4044-a2ae-a4dbfa4050fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 195,
                "y": 162
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5bd90d22-7795-41dd-bea6-42da8f59b765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 19,
                "y": 194
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5b4b69bf-0e63-4fba-89f0-b6e9e970dbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 143,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "22d3f485-ab19-4a01-a2a2-ffd522b75857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 173,
                "y": 34
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5609d807-4805-4b3e-9a44-790bbfad213d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "74857117-8d3b-46e8-9ca0-01ad992f9144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f5d3c29b-2ece-407a-9c17-24a9ffaa011b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 2,
                "shift": 5,
                "w": 5,
                "x": 39,
                "y": 194
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "40f660d7-8c7e-4d4c-a9b4-63259d8a1b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 211,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0dbd2cca-9b1a-46d4-bcd7-fcd81d26fd71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 223,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7e2398e2-79f6-4007-bb7d-34dd6139dd80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 172,
                "y": 162
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d19d7e12-6fad-45a2-a99d-d5db942a43b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 171,
                "y": 130
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "90254216-e8ba-42f4-a033-5194a05053c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 186,
                "y": 162
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d3b3c008-39ce-46f9-a358-d710a6470e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 10,
                "y": 194
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cb82f7db-0f00-4ad1-ae41-bf0aa0e368be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "155091bc-c104-4d9a-9422-ad8c9d9b22ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": -4,
                "shift": 12,
                "w": 18,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f3855b1a-e76d-4ebb-8517-82ea1af68917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 92,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "39f96801-aa91-4188-b1ba-60f4ca1f51e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 219,
                "y": 66
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1b4925bd-2c66-4be3-9f6f-182d29e58209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 237,
                "y": 66
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b2761095-5adb-4efd-ac1e-0552b7196b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ee3f86a3-0d8f-4d44-8e6a-82f023b46b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "da7581d9-348c-49f0-931f-d09d6720d837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a2164b5b-8147-4092-a11f-897377c16232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 160,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f17a26b7-8c3b-48b9-a251-6a9508b4e2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 130
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a6cd13ea-7468-4401-b563-87bd00c4764a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0fda84d1-63d9-4836-9870-df8b375620ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 74,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f28710ff-49c4-432e-92d2-dea6cc27200a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 235,
                "y": 162
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "025a7548-43d8-4356-9c4c-5a21f47943bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 137,
                "y": 162
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a5784a6f-a146-4d32-b38f-ebd5d7ce9085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 26,
                "y": 162
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b6f073a8-1bc6-4499-9101-a317514b8f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 56,
                "y": 162
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bba8458a-2ab6-4683-989a-e56ed6543772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 41,
                "y": 162
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "12477b63-e426-4a1d-86e1-5a47f11d589b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 130
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "00ebd81f-a375-4d80-b605-d559b85b3bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d9210e73-475e-46a4-8077-f5e65224b19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 40,
                "y": 66
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7d41e7aa-b7d6-44c7-933d-c1b4fd6e8521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 136,
                "y": 34
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "19912eac-97fc-4df6-923f-6484a1dc7af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 75,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b68c7bf9-ffc8-482a-96e6-ad3a08ba71d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 191,
                "y": 34
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e053e4bf-bc57-41f4-93ac-b8b6ea17b601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 17,
                "x": 210,
                "y": 34
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dfee42ff-4d13-44c8-be6e-3b28908815e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 165,
                "y": 66
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6b190eb9-9442-4f38-ac44-126eb3bf2f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 21,
                "y": 66
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5c077805-ab23-43db-809b-6643fcb59467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d34a12f4-57d0-4208-8c64-0338aa9b8f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 225,
                "y": 162
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8abf6a91-d437-4892-8b81-ef471c739309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 235,
                "y": 130
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6afa5775-91d1-4144-80f4-7b9f28583648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 39,
                "y": 34
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ce5aa347-c261-498a-bd73-cd5a9c147a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 111,
                "y": 130
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "96b7f79b-12ae-4f92-9500-4b5957788ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "77a7122a-69f4-4dc7-93cc-f213482fa7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 99,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3c16e346-6c9b-4327-a18a-03fc5414376e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 183,
                "y": 66
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "44b555fa-799b-47be-ae4c-8c38a3d7cce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 229,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "11de2f19-006c-430d-b030-1e98a095b2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "205cf4e2-1a3b-4300-a440-a2a873f60dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 79,
                "y": 34
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "419193be-eb5f-471d-bc92-b6852cbe1f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 147,
                "y": 66
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d1bb6712-13e2-4803-8879-4a5120a1a0ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 15,
                "x": 109,
                "y": 98
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "69ff465f-fab1-4cf8-82a2-5990c38d6272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5d8b8335-4d3e-494f-a0ec-a94e138d7f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 111,
                "y": 66
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8973b005-ac50-4203-8a68-92bb6ac460a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 1,
                "shift": 24,
                "w": 25,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6ef4fd88-c800-4b04-997f-75416d367a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fea9b8f8-3b5a-4858-a4e7-08dec2121632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 16,
                "x": 93,
                "y": 66
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "1d20658a-b173-44db-84c6-47348b172541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": -1,
                "shift": 15,
                "w": 18,
                "x": 59,
                "y": 34
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40e90f02-e80c-430c-af5e-cd6e73109d7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 199,
                "y": 130
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3a7a994c-1c12-4314-abb4-17d23bda425c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 187,
                "y": 130
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ef3d9483-d502-4454-b2bb-36118cd3ec65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 14,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a1e48f9e-4f56-4c7a-b10a-b8d99ac12da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "57e111ef-9e27-456c-afba-9cd618f479ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 59,
                "y": 66
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "176d8c6f-4391-4c47-9250-4f0d859094d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 5,
                "shift": 14,
                "w": 7,
                "x": 30,
                "y": 194
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d08e05ea-4e94-4381-9718-0f1d212dc43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 130
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0615d211-aa16-4ef4-8ef7-bf10b16cd9a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 130
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0062ba00-056c-488f-8ac0-19a0afd218c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2b4d2584-6f3d-4e72-b0fa-3faf1a751191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 126,
                "y": 98
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2ab14835-6974-4482-bab9-e2f02b342f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 209,
                "y": 98
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "664ecbfa-5cd7-442f-b29d-4a47646a48e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 72,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2a0f45db-b3bf-47e7-9362-3dc9c33adb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "09d46612-84f9-4651-b014-340454850545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 130
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "294aac42-68a8-44de-af9c-b715ee43597d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 215,
                "y": 162
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c0ff1d9a-b7bb-488a-a195-6f7e3b5edae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -3,
                "shift": 7,
                "w": 11,
                "x": 98,
                "y": 130
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "73794b22-961b-4090-9d2d-f5fc25c27e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 129,
                "y": 66
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "47705624-31cb-4ddb-9b5a-adccec3cdf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 205,
                "y": 162
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "571d4927-7ab5-4d0f-82d0-a0703eb9e4bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f1fc569b-c19b-4759-864d-b18b82ef2c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 193,
                "y": 98
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e1c21882-0174-4786-95b3-949cd2552da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "98c8b3ad-a021-4118-84ed-e24e47c4c666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 119,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e5133335-8154-4850-932b-a25833d4f3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 22,
                "y": 34
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "94a605a3-c8bd-4c0d-a157-b6b8dee5eb0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 162
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3f1a904a-178a-4feb-8dd9-f3769bfc5f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 126,
                "y": 130
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cbf7d7a8-45ba-44e1-b3bc-51349053361c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 85,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a53a5cd3-55d8-4526-9d1b-44b44a9e3bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 141,
                "y": 130
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2cfc458a-5776-44af-a45e-f559a541c41d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 225,
                "y": 98
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b501ede1-d14e-45a9-a002-3823eed2b5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 20,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "57cebcc6-0c3e-49b5-bdbb-bb33aab35ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": -2,
                "shift": 13,
                "w": 16,
                "x": 201,
                "y": 66
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c604a918-8aad-44af-9942-e8e4222ca57f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -1,
                "shift": 12,
                "w": 15,
                "x": 156,
                "y": 34
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "459dfe86-c1c7-42f8-b111-2214c1d86c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 177,
                "y": 98
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4947c3d9-61bc-4aa5-98cd-2030db651979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "94646745-c025-473c-967b-ae294ce11c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 244,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0cbe95b5-a5e8-4c16-b3db-c08a1994911a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 98,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "348cb915-2854-4d98-aded-eb8b20ba4885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 109,
                "y": 162
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": true,
    "kerningPairs": [
        {
            "id": "35ebfe19-beb1-450a-933c-56f090a5e9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 65
        },
        {
            "id": "a3e33930-c986-4d69-b09b-cb45ed156a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "1381d557-008d-4ae4-98f7-42095b36b1a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "c55ba82a-09d2-487f-a21d-e997c2eb97be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 1040
        },
        {
            "id": "dac414f9-8886-4040-acb6-608e8e3a796c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1058
        },
        {
            "id": "f84dcbec-d214-409b-be90-fd91ad2a5165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 49
        },
        {
            "id": "9eae5194-49ee-4c1e-a674-15e5bca47406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 55
        },
        {
            "id": "dcb69ce7-1b33-46f8-aa50-ff6bb5abee67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 44
        },
        {
            "id": "f2a206ed-8ec3-408b-af0a-fe3742782092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 46
        },
        {
            "id": "346913ed-eac9-42d2-831a-0181fb07fed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 54
        },
        {
            "id": "c0f75ac1-74e7-486a-80e5-953eda8cd2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 55
        },
        {
            "id": "92cb21c5-3a83-4611-ae5c-ffa8e7e4b09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 49
        },
        {
            "id": "7e9be3de-5a0b-4eb7-a571-dc5e374d505a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 55
        },
        {
            "id": "af49b8d0-7bb4-4755-92ae-d3fef2949de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 49
        },
        {
            "id": "562de721-3311-42a1-9d35-ddb17c459f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 55
        },
        {
            "id": "e8f2e463-f941-40b2-9070-cd9451ae1f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 49
        },
        {
            "id": "a8acd0c8-16f9-4ab2-8e6d-31fe12259013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "69b4995e-b776-46bf-adfa-56765f0f8f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 49
        },
        {
            "id": "7fde4320-35cf-47f1-a078-704b2182fcdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 55
        },
        {
            "id": "4b1292b8-1c84-4207-9a2e-10ad7908fd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 49
        },
        {
            "id": "8186469e-6ae0-4480-98ae-cefec746c79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 46
        },
        {
            "id": "9b28993b-61ed-4c5b-9f96-c14c320a7b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 49
        },
        {
            "id": "50943557-4926-41df-b94e-ddedc2885bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 52
        },
        {
            "id": "09b7763e-aefe-44f1-8e91-d7112987767f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 54
        },
        {
            "id": "60f79519-6fae-46a8-83b5-42f2b4418fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 55
        },
        {
            "id": "c3af83cc-2f78-479d-9e6a-979c9209addd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "4b02f348-ed07-4d0e-9a4d-34d8abec4f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 49
        },
        {
            "id": "5810dfd8-2575-493e-ad4d-bd61adac5b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 55
        },
        {
            "id": "bc574a05-1281-4d50-bb09-fda065102c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 49
        },
        {
            "id": "6899620e-7ab8-499e-b798-e8440912807b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 55
        },
        {
            "id": "71efc70c-8123-46e6-b7fe-0e3872139c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "f6729746-8bac-44cd-b579-c019b1db3a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 45
        },
        {
            "id": "81d98e28-c9e1-4abb-bf16-e15af57965f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 46
        },
        {
            "id": "5c49d4ce-fb3a-41da-9c54-e799c7f41de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "1dd90406-13d0-46b0-bf3a-3a054e0b8c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "27d32954-275b-4200-8c0f-349565f7b2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "f04f67f7-81bd-4a06-9323-1bd90279f197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "630377fd-8077-4615-b300-f108e023e52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "0a4dd649-16d2-4bf3-b3fb-6420fb99aa46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "04605d9f-58c5-4758-a3e8-7562e7adb753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ec53af9d-8445-4827-9c45-98ddbbd3b6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "9e9184ef-329f-4351-9056-966dd3ceb92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "2d66aecc-6b08-4eff-bcb7-775e90425eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "004a0c8d-24e3-4e0e-b073-83bb10942da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "eb28c7b7-0f98-40fb-86e7-5f057b460aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "c7be50b6-7846-407b-b010-048b0124d8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "a4da01b1-b066-4d3b-8356-f5cbacd6ac93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "e9b34ffc-f64e-43bc-9fb1-8508a7d88311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "abe00290-6eac-496e-9b7c-48ad8a9052bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 45
        },
        {
            "id": "5d9a60ca-3f64-42c1-bf4c-6274cacf4b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "da970c26-e029-40a4-bf7f-bb9334ef6ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "354b8b4d-55dd-4703-a7b6-2ac28d3c9e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "67332a38-27bd-40a6-ae4a-abe4e94ef883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "84b2bf9e-f6e8-46f1-9930-87dfe1c1b55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "6c42d246-b2ef-454d-84e7-7d97b6078d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "53b3caac-16f8-410e-868c-e4a61559eb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "ece28f7d-259a-48fc-a2d9-6a18bd30fb5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "3469a66b-7e58-47a4-afe5-10d2b65c3d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "84c295d3-9483-4eee-82c3-44814db8232c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "6aba995f-56ff-4168-91b2-aa264b164fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "5423bd90-6519-4d2f-9d56-b996a00204c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "5c556043-09d1-4209-8646-701a70659f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "0c5cbf15-e2cf-4874-9b46-b4daca05fb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "277e5f31-2f45-48db-9ca4-f17669cf98d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "69e1c0e7-330d-4c23-acee-33d8cd1bb855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "835bcee2-509b-4beb-91ff-e8a4dc9fd981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "84270fbd-fdd9-42a1-acc5-95f57f26339b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "bcfb3e7c-a020-483a-89df-582c8b0dac06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "327e2f9c-c27e-459d-839b-9a293735732e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "11d5d9e7-f5d3-43a2-80a7-9218f1114a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "13303fbb-0411-495d-a24c-dc03a0650ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "63c09a3f-6db8-4a63-ad2a-9befbfefbcac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "4a49524d-16fa-4923-bd44-bf7cee049954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "af294ffb-c728-4391-a354-111b48723256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "ef91ba0a-61d6-40e9-8164-ded477f7812b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "c66d26f6-84d9-4ace-804d-58aa9be54832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "d6a0fc6c-26ae-4a93-ab02-58b3879d2118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "612acf19-7711-4f70-86fd-cfc002f1800c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "b6dadb1c-8ad1-48be-9351-66ff3990fe00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "9cc4e09c-96b1-450e-9caa-fb40ba39e97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "dfd90082-ce3a-4bec-bf56-e2a83aca5dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2e036ded-c857-4677-b14c-a62d61350701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "52a7f8eb-b317-47c5-ab64-64b7d6bfafb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "04a133be-ae52-4252-ad1f-166e7da6af89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "65a1d057-0618-4186-98ce-a271722441cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "e57ebfec-6971-41d9-8c19-db6c9e2d4cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "3cd22480-6ccb-4af0-b269-4b21e44bb50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "a69a1830-bbe0-499f-95dc-6f1805d4fcac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 83
        },
        {
            "id": "3bca4028-4047-45b4-8942-cbbaa6415127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "82e48e85-ce31-4716-b4b5-f22b86424014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "54e319e5-47cf-493d-b645-0b20893993ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "e7ef5aee-eed7-4395-9d25-176edfa997fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "7550416b-b014-441f-96dd-a4a8be34f19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "88c53cbc-302a-4d04-818c-0dfcf45a7b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "04d3d86a-334b-4fd2-96eb-8c82988cf6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "ef92ebe7-30b8-4b86-b064-21e373002b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "e38a5676-3436-48d4-8866-90291264ad8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "7f23de17-f2d0-4d99-a50c-de4f52b3442a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "b433f990-b48b-4b08-8e77-16c728a9761d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "d9920c20-8cf3-441c-8ff4-cf57607f4d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "737a9a10-95fe-432b-ba30-8af628a89fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "40bde57b-c666-4799-8aa1-a74831905ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "e76b446f-3483-49e5-89e0-46e0f3886fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "548587d8-86bc-4661-a126-18e6145f9733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "cf7ea7c5-7149-4ff5-9b15-b4030cdf1276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7f3ada73-4ab1-4f07-a348-8fe5420bc7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "26b6defa-85a1-4dec-acfe-dbd9eae55fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "64efc882-3441-4d90-8633-59eb95175407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "794a5043-9dda-4f9c-bcc7-d4b111cd7ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "b5aab1b6-f753-4fff-96cd-b0b531a66d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "0b38e641-76d2-42e8-86ba-f737948df6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "0efd78aa-5702-48dd-aa81-196c3da5ec3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "9853859c-4cff-4a99-9f54-2695f5a525ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "e1331080-abbc-4a93-b7f3-99d17694fe75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "f1c98816-b518-42b2-a9ee-eaac201462cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "75957239-15ac-4bec-82f9-8eb0477b9d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "ed95a293-2777-4a63-af54-e7f58b84eed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "06d1af11-7d8c-4c08-81f7-aacfb9533182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "05ddcc47-fe93-455b-8021-18f6fdbfd675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 84
        },
        {
            "id": "3791986d-7532-4654-b995-61d062877d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "fe4518ae-8cb2-456a-b8c3-7146d528cec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "64cda1c3-ba52-4deb-b693-a3c0fc9f53f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "6d7902cf-72d6-4721-a685-a57b290c42d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "7757015e-1ca1-4d3e-9917-64eef035ffe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "66da5633-3745-4553-9bc4-6314a1a81dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "b3f50999-7efd-4759-89b0-e3a3a825fe5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "d571887a-9224-4f41-ade8-4bab41685465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "e83752db-f21b-4c84-8623-713273f1a495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 101
        },
        {
            "id": "ec6da567-f070-4a31-9fe7-8eae0ec020a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "0b3df7af-9d4f-4c98-b16b-58e2eb1dd00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "fac41198-a115-4bd8-b8d5-b918198b8549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "79111cab-041f-424a-84ce-ef6e74bbf798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "57ebff1f-2b2e-486c-ac90-e94d7dc77938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "5e2b6d2f-60fb-485a-a71e-47fd1fbe4e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "67af1e65-a408-4de3-b1f2-3b8d796483e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "e362faad-94b9-42b4-b3b5-636e5482504d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "66631cbd-4013-4e4b-b972-ef19d04afe72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "1ab67ec9-264e-4bf2-b876-8984d4a9c046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "23a0ed94-ae15-475b-b4cc-87683f98b3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "80e82099-0b01-43f1-86ad-a3651abd722b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "d0c6e7e3-830a-4e23-89dd-83171141bdf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "23724d81-2c4f-4135-8b70-fa1ec421845e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "8a1cd64c-b1ae-4859-8193-94bd413b26d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "5f80b063-f58f-4be5-9369-f95d88f67755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "cc0e98e1-0ea2-439e-9708-a6db9ccd6aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "5f4772bd-7ba7-478e-9c25-83f90ad8f4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "80b66e69-7435-4f96-affa-c06350d92805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "f8a25a8d-7e0c-45ee-8345-f94f59e2b517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 102
        },
        {
            "id": "48dc27c2-5787-4732-8e92-ccfa4f4cd42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "ecc8fae0-cacf-42f9-b5b6-47836e149e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "c19d165f-4e19-4a16-b754-1a15fd5081a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "6dff0b91-3c54-4602-a53f-20778a69be0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "1e8cc02d-d175-4b4e-9775-83a045a069e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "5dd97336-b817-4dab-80aa-e50507bba123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "5ac06195-d950-4801-9d2d-207b269a2b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "c8edf234-6b64-4120-a7cb-041eb4bd17f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "bdb81baa-a307-4f8a-abf8-15f49f8371eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "aaccec68-2551-4e2f-a7ca-ced94ccef8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "ab955d5f-bf06-4ce3-9c25-0f13307122eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 45
        },
        {
            "id": "d8b77dd3-490f-45ad-a7bb-a3f345c48161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "6aa8bab8-424d-4bf7-9840-8aa3f2b99b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "295664f6-fa29-4b7d-9f2d-7301e7c10738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 45
        },
        {
            "id": "b5681562-540a-4ee5-b800-b539b71f1cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}