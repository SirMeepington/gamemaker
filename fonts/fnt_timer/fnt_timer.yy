{
    "id": "d4f1619a-1e50-4a1c-8ea0-9aae0b1b4db8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_timer",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Century Gothic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ebef6483-89ec-4f7a-bd22-0402f1a383e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 219,
                "y": 114
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "89a16f90-79b4-4595-b52f-567332c4b40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 203,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7dc90da6-5cab-4413-991d-04cd7d16ae4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 114
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "086f83a5-c25d-4bfc-93ad-f858adcd6836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1b68350a-15c2-4eba-8bde-2ecadc817754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 12,
                "x": 102,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "038e143d-80fb-4a0c-b540-eb82db96cc81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2731cce3-458f-4a47-a74c-3d4b1e1dafe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 35,
                "y": 30
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2aef3673-e3cd-422c-abc5-f5bc7c344dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "522c2752-4d72-46f6-94b4-8b95703e3c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 50,
                "y": 114
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "94a091aa-135a-4fb5-be71-2cd6e0142434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": -3,
                "shift": 8,
                "w": 9,
                "x": 61,
                "y": 114
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5b11338b-8ec3-4ba0-8365-41e5bd1501ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 114
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c95ce404-7976-4127-a039-25c61e535e32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2798859c-492d-4062-8997-6dbe8ebedfe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 227,
                "y": 114
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e54d8ecc-71f1-41dc-999f-e442080c7fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 162,
                "y": 114
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5b66ed0f-3141-4a76-bde5-86fedf9dd0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 248,
                "y": 114
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d1592136-6d74-463b-9a24-5eceb87f73e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 114
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a9ae7020-7de9-4f11-88a6-7e50bfb31822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 86
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3de2c8b0-5c20-4bf6-a438-60ac4ee8a9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 195,
                "y": 114
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "06771357-7647-490f-94b8-aa5ac2910e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 86
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "63203622-6af0-4169-a2a7-893680c44cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 86
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "14eb9a61-40dc-4d74-b570-1edd0874be17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "302f0341-cd37-4e01-b8f4-7218826c007d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 130,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1bf87e3e-77af-403f-b858-e3251fa0e0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c0d18073-9eec-4170-a2f5-a1c39ce2dcc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c090010e-3957-4f03-8614-090e5155fea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2cbeec63-2275-4d9a-946a-430f1c0f31ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8bdfc668-af6b-4196-8790-f3340ca88616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "724f1d91-ccc5-4854-9f93-e45fa24998dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 187,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d18b0fc1-1a43-469f-8bb4-9dd50860780f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 119,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cbe6350d-411d-4880-b18d-11c513f894ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d1b6065b-c3d2-459a-b2bf-bbc602d644a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0e9bbe27-dd10-463d-b3fb-1101bc595a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 158,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "03ee6b87-9e6a-49dc-968a-0d5af0dc705e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "283d9d6e-55d6-42d9-8e43-4ce5c7c73acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1c9f0cb9-3501-41de-be19-97d09b4442e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3f22305e-5e7d-4eb4-a11c-02c87eb6f645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "02956b49-3576-4024-9847-d74f6b411ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 115,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4fcc4901-f73d-4b6c-926b-e27ff7c6a999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 184,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5ee5c473-fd9c-434f-887e-6a1c6770aa15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c4e85b6f-9a89-4342-98a9-01684369cf59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fc1415a4-e8dc-4f54-92a6-79359a3051d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 99,
                "y": 30
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "44e51ca7-e63e-4334-876a-28bc4d16ddbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 211,
                "y": 114
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e8feb0cc-8c55-429e-b7cd-441d90d4b237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 197,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b5149878-0407-49fb-b422-4068393f2c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 83,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "44170456-7fc5-494d-ad3b-58ea85bbc107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 114
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "081ca2ae-cfab-4ec5-b66b-e1bceabd671c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a6138dba-c48d-44df-b666-6abf13934f9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "16097385-59ec-4104-a870-d2d31884f888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0388312a-6914-485c-adef-9b3b4e847efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 46,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "080bb83a-0c80-41bc-a7e4-aded720c50fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd7314d1-966d-4428-bc84-b59734194783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 60,
                "y": 58
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "edc96626-238f-4b57-a883-ea542b2f7f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 32,
                "y": 58
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9b821089-1df4-42e0-bfad-93469b111e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 186,
                "y": 58
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "26cf53ed-9c0f-4249-a646-97de7e23d580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 158,
                "y": 58
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7f43353d-3b44-4781-b347-776d4e67e9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "47aaa48c-6257-48ec-823e-63edf2396b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "71d708ae-c6e3-4dd3-a151-5c1aceee00e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f011ac86-984e-4db1-b454-97de23bf1e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 51,
                "y": 30
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "84ec6eae-bf72-4792-976c-f9aabfda402b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 212,
                "y": 58
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fbf9390b-0903-4a25-b7ec-ed5b288f63c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 114,
                "y": 114
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c333f941-14ec-4be6-bdf5-da342198fcf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 38,
                "y": 114
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4d424c35-2c5b-4f37-a71a-face40f38d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": -2,
                "shift": 7,
                "w": 8,
                "x": 124,
                "y": 114
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5536aaf9-28ba-4d63-b3fd-bbc5200ffff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 58
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "898fe3ab-ed22-4f44-9f74-110e9bc1ab7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 116,
                "y": 58
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "44cfdc8d-d45a-4a0c-979d-d27543a16068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 171,
                "y": 114
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3e48d561-c2dc-49df-9fa7-7af44d6b711f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 191,
                "y": 30
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b99a4be2-ab3d-4721-b927-5eadbbd61470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 131,
                "y": 30
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "48141eb3-f4e6-4c5c-bab2-022f3a6b1e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 221,
                "y": 30
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4ca85edb-f694-4918-b589-21325230c03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 67,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a3c93322-d241-4f75-9f24-31002df8afe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 236,
                "y": 30
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0e25a8a2-d22b-48e7-9249-108b704e5a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 153,
                "y": 114
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9b7edb2d-f715-4797-ba7c-258322ae0627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "00925b9c-69cc-4196-ac34-6ec2e2eaa452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 144,
                "y": 58
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "40b993a3-024d-4c32-bd8f-56aa4ccc549e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 179,
                "y": 114
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3a622063-c6e3-44a5-a4f4-d24d31f5c90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -2,
                "shift": 5,
                "w": 8,
                "x": 94,
                "y": 114
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4e178700-0df6-4c1a-af46-7274bde5f61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 17,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1b6fc3e0-e416-4ab8-8072-abb2672feae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 241,
                "y": 114
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "719c70d9-327c-4935-80f3-e8c0491b02c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a97180c5-63e7-4023-b6b8-0d8b31a200e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "91ccb86b-8ea2-4535-b6e3-21954be9bf37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 161,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "40f2bea2-f67d-4ca8-9649-f3c2cf0950e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 19,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e76491da-ddd6-4226-bcad-9ade0b9b3aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 206,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9bb6fa80-a54a-4c85-850a-04bf624e331d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 134,
                "y": 114
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "79cbca1b-1bee-45cd-bc32-2a702de4e882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 235,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "54491e19-af82-4ef8-b8f2-bec2d7589493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 144,
                "y": 114
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6a8ad5b4-3bbf-442d-a042-365122958fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 199,
                "y": 58
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3c1eaaad-e7e3-4969-8642-31a13faae00e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 88,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0b97dca2-eb28-413a-9897-965041390c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d197c2d4-e10f-4563-9bf9-57518ce61e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 146,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c247072c-6b46-4663-92b0-ddae0b16ba8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 176,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6db9e725-30c8-4dcd-9f26-7145418d766b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 238,
                "y": 58
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cab956ee-fab2-46d0-b8a7-2bb8ddf712b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": -1,
                "shift": 7,
                "w": 10,
                "x": 14,
                "y": 114
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "86302cfa-a184-4e74-bf73-24cbbff8a461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 8,
                "y": 142
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bca529a2-e40c-4c7c-bc4d-3759e82daadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": -3,
                "shift": 7,
                "w": 10,
                "x": 223,
                "y": 86
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "40bcdacd-17b9-4a64-a2d4-e657614866fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 86
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": true,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Bold Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}