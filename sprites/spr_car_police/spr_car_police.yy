{
    "id": "56ef9467-0c80-401c-874e-94df89b92fb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_police",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 23,
    "bbox_right": 100,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 148,
    "frames": [
        {
            "id": "d9ca5587-6a7c-4054-bac3-2d682f3a11fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ef9467-0c80-401c-874e-94df89b92fb2",
            "compositeImage": {
                "id": "fa7ff844-620e-4de8-9099-ab4203fd7daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ca5587-6a7c-4054-bac3-2d682f3a11fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58dce3b-1a6b-4573-8fa6-abe71a7bfc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ca5587-6a7c-4054-bac3-2d682f3a11fe",
                    "LayerId": "6cffc1a7-0aac-410c-9519-10b1defd0469"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6cffc1a7-0aac-410c-9519-10b1defd0469",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56ef9467-0c80-401c-874e-94df89b92fb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 55
}