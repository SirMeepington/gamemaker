{
    "id": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_retrybutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a5c2023b-2fd5-4d3e-992f-e8ddcc183e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
            "compositeImage": {
                "id": "d46f295a-ebc7-42be-824b-a6dda48cf575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c2023b-2fd5-4d3e-992f-e8ddcc183e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c49af8b-fd9e-447c-877f-4a4da12fbdfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c2023b-2fd5-4d3e-992f-e8ddcc183e86",
                    "LayerId": "21c2c8a6-820c-4b2d-9874-2979f286f37f"
                }
            ]
        },
        {
            "id": "456b573f-fd09-4a35-99c2-d7a17cb471e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
            "compositeImage": {
                "id": "ac9c5c1d-6e7e-4dad-80c3-785e288bf401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456b573f-fd09-4a35-99c2-d7a17cb471e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452f40e8-e60f-4191-a01d-6a3eb1168615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456b573f-fd09-4a35-99c2-d7a17cb471e5",
                    "LayerId": "21c2c8a6-820c-4b2d-9874-2979f286f37f"
                }
            ]
        },
        {
            "id": "32e4dbfd-70be-49b1-86e1-f2a515ece259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
            "compositeImage": {
                "id": "87600e2e-b43a-4216-9377-5257be0491a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e4dbfd-70be-49b1-86e1-f2a515ece259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd49096a-8969-4b39-8cbf-52a7ddf0f12e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e4dbfd-70be-49b1-86e1-f2a515ece259",
                    "LayerId": "21c2c8a6-820c-4b2d-9874-2979f286f37f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "21c2c8a6-820c-4b2d-9874-2979f286f37f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 34
}