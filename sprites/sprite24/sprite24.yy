{
    "id": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 11,
    "bbox_right": 51,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d983fd12-3fd7-4892-a259-0c86d214bcc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
            "compositeImage": {
                "id": "b743ea93-9485-43bd-8987-ab8cfe1ea56c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d983fd12-3fd7-4892-a259-0c86d214bcc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2afca43f-6e99-402c-a2e7-6c1ff7bf4e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d983fd12-3fd7-4892-a259-0c86d214bcc8",
                    "LayerId": "5ebfd053-bb07-478e-aacf-0396f4ebb223"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5ebfd053-bb07-478e-aacf-0396f4ebb223",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}