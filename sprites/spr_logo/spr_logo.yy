{
    "id": "2f29e153-26f3-40c1-9d68-8574ceca8dfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 76,
    "bbox_right": 431,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2744f104-538e-4d7d-8466-9ab449111df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f29e153-26f3-40c1-9d68-8574ceca8dfc",
            "compositeImage": {
                "id": "5ec3bcd0-6f3c-48b9-b962-657bf8f6f7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2744f104-538e-4d7d-8466-9ab449111df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e372789b-3762-476d-a6e9-c444142cf5f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2744f104-538e-4d7d-8466-9ab449111df3",
                    "LayerId": "9828c03e-b63d-43c6-84f1-97d3d65c5cef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9828c03e-b63d-43c6-84f1-97d3d65c5cef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f29e153-26f3-40c1-9d68-8574ceca8dfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 128
}