{
    "id": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_helicopter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 24,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9a2139fc-6979-41c3-9cf8-7e87a2ecd281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
            "compositeImage": {
                "id": "47c5026c-5f02-4d84-9a7d-8808f44f7832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2139fc-6979-41c3-9cf8-7e87a2ecd281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf34530-9f6b-4e17-a4ba-9c4caa2bd8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2139fc-6979-41c3-9cf8-7e87a2ecd281",
                    "LayerId": "e5d2a1e5-d331-48a5-a9d7-70897f9ca176"
                }
            ]
        },
        {
            "id": "9c3c971a-1edb-40ce-b11a-74703f177b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
            "compositeImage": {
                "id": "0a16a0e9-7d64-4ac3-a107-ffced15c76cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3c971a-1edb-40ce-b11a-74703f177b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e63350e9-5465-4db7-aef0-82162dd2961d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3c971a-1edb-40ce-b11a-74703f177b38",
                    "LayerId": "e5d2a1e5-d331-48a5-a9d7-70897f9ca176"
                }
            ]
        },
        {
            "id": "d8cc6b5f-c734-4192-a81d-7c2f30994038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
            "compositeImage": {
                "id": "dd8fe2b9-97fb-4318-96d4-b2d396a215af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cc6b5f-c734-4192-a81d-7c2f30994038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb95fd8-78a4-471f-94cf-a751fd54cec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cc6b5f-c734-4192-a81d-7c2f30994038",
                    "LayerId": "e5d2a1e5-d331-48a5-a9d7-70897f9ca176"
                }
            ]
        },
        {
            "id": "3e304568-0741-4aa4-9844-72f2fd59681d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
            "compositeImage": {
                "id": "7e09d4e3-e818-47f0-95fb-8b2d659f4e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e304568-0741-4aa4-9844-72f2fd59681d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed45188-1a12-4fc8-8e98-c8611199b2b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e304568-0741-4aa4-9844-72f2fd59681d",
                    "LayerId": "e5d2a1e5-d331-48a5-a9d7-70897f9ca176"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e5d2a1e5-d331-48a5-a9d7-70897f9ca176",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 67,
    "yorig": 70
}