{
    "id": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powerup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c82602ad-af26-455a-83f3-4871b4b36c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
            "compositeImage": {
                "id": "53c799dc-608b-427d-91d4-fb0067ce33e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82602ad-af26-455a-83f3-4871b4b36c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8a13488-92fb-4559-a244-be9b898716e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82602ad-af26-455a-83f3-4871b4b36c68",
                    "LayerId": "fdaae1ac-0062-48d9-8a4c-72714f2de370"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdaae1ac-0062-48d9-8a4c-72714f2de370",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}