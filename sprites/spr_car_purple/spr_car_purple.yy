{
    "id": "3c8de1b4-2856-4294-b5dc-02e09fc7d119",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_purple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "1724e729-9f2f-48f3-975d-4c8ccbbb3a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c8de1b4-2856-4294-b5dc-02e09fc7d119",
            "compositeImage": {
                "id": "786df5cb-e2c7-4287-afae-352bbc878f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1724e729-9f2f-48f3-975d-4c8ccbbb3a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e5f094-0de0-4649-81a7-c09bff42930b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1724e729-9f2f-48f3-975d-4c8ccbbb3a93",
                    "LayerId": "a5069acc-88a5-43fb-9361-0bc238d2c2ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a5069acc-88a5-43fb-9361-0bc238d2c2ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c8de1b4-2856-4294-b5dc-02e09fc7d119",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}