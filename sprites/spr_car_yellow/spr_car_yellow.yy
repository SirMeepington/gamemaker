{
    "id": "a7d5deeb-829d-4d40-9121-15b92fe6fe80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "3d262f86-a20d-4628-9582-37ae21c525b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7d5deeb-829d-4d40-9121-15b92fe6fe80",
            "compositeImage": {
                "id": "835b6046-abcb-4fe0-903c-2d10491be03a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d262f86-a20d-4628-9582-37ae21c525b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd29c27d-a0d7-4ddf-b466-b51bf4de508d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d262f86-a20d-4628-9582-37ae21c525b1",
                    "LayerId": "6399d7ea-0519-4c38-b959-141fa7336ec7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6399d7ea-0519-4c38-b959-141fa7336ec7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7d5deeb-829d-4d40-9121-15b92fe6fe80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}