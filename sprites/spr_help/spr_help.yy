{
    "id": "31b7aeb5-934e-4f05-bd4a-bb86b626387a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "31bd1003-d60a-46c2-9b5b-e282c5a9581a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b7aeb5-934e-4f05-bd4a-bb86b626387a",
            "compositeImage": {
                "id": "c8c135e2-03ac-42ec-a5a9-8482462ffa00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31bd1003-d60a-46c2-9b5b-e282c5a9581a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005153c7-b6b9-4ffe-9a9e-b6e60ec13b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31bd1003-d60a-46c2-9b5b-e282c5a9581a",
                    "LayerId": "8bec8427-dfa5-4b9f-9b8e-ab67f741fafd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "8bec8427-dfa5-4b9f-9b8e-ab67f741fafd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31b7aeb5-934e-4f05-bd4a-bb86b626387a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 640
}