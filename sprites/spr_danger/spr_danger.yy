{
    "id": "54a090aa-25ac-4b2b-92a0-31b321dc23bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_danger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f59dab81-6186-41c7-a5c0-b8326723255f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a090aa-25ac-4b2b-92a0-31b321dc23bf",
            "compositeImage": {
                "id": "79688ba3-a399-42f3-886c-d2f149402fd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f59dab81-6186-41c7-a5c0-b8326723255f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef587a72-9a85-4fca-94a8-d22ccea2ca42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f59dab81-6186-41c7-a5c0-b8326723255f",
                    "LayerId": "acb276c0-324f-46b8-b47c-e7cad074265d"
                }
            ]
        },
        {
            "id": "1f64d5f9-987c-47b9-b238-1d80eff72095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a090aa-25ac-4b2b-92a0-31b321dc23bf",
            "compositeImage": {
                "id": "8a6506c1-e8e9-4cc3-9d33-bfaec29057cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f64d5f9-987c-47b9-b238-1d80eff72095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e82536-6b15-479d-8b8d-fd9dccf296e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f64d5f9-987c-47b9-b238-1d80eff72095",
                    "LayerId": "acb276c0-324f-46b8-b47c-e7cad074265d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "acb276c0-324f-46b8-b47c-e7cad074265d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a090aa-25ac-4b2b-92a0-31b321dc23bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}