{
    "id": "7affd203-f3aa-40f3-983a-d9e25ced224d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "d9f0e4f7-d596-4ecf-90a1-98f11e3228c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7affd203-f3aa-40f3-983a-d9e25ced224d",
            "compositeImage": {
                "id": "fc19235b-caae-45d6-a8e0-17e34aa2831c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f0e4f7-d596-4ecf-90a1-98f11e3228c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0489cee2-a60a-4c2e-b724-4ce362790cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f0e4f7-d596-4ecf-90a1-98f11e3228c1",
                    "LayerId": "06615bed-301a-4800-834f-47dc95090d46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "06615bed-301a-4800-834f-47dc95090d46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7affd203-f3aa-40f3-983a-d9e25ced224d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}