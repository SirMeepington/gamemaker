{
    "id": "5a5d98cf-cc24-42b9-8f43-4d11640ba658",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "025c1021-fd97-4ee3-b2a9-98c2b12bbdaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5d98cf-cc24-42b9-8f43-4d11640ba658",
            "compositeImage": {
                "id": "91a494b3-58c7-4ad7-840c-4b152ff6d36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025c1021-fd97-4ee3-b2a9-98c2b12bbdaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c581c83c-5ace-4506-89a4-1bf5548db9fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025c1021-fd97-4ee3-b2a9-98c2b12bbdaf",
                    "LayerId": "0d82199a-a63e-4a85-a154-8d8a38e17479"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0d82199a-a63e-4a85-a154-8d8a38e17479",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a5d98cf-cc24-42b9-8f43-4d11640ba658",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}