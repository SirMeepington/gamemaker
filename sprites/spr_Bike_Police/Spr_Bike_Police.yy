{
    "id": "d3c6318b-5d0d-46dd-bb40-17b91d1c1863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bike_police",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 48,
    "bbox_right": 79,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 244,
    "frames": [
        {
            "id": "85240253-0fda-43f2-b715-2bf0ca5cccdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c6318b-5d0d-46dd-bb40-17b91d1c1863",
            "compositeImage": {
                "id": "50f409f7-d469-479c-b9a2-e909e0a83bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85240253-0fda-43f2-b715-2bf0ca5cccdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "372dcaba-0a22-4f91-aca2-dbfdc3e593ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85240253-0fda-43f2-b715-2bf0ca5cccdd",
                    "LayerId": "e4c23936-d715-4e77-9f40-7fbeda1c73a1"
                }
            ]
        },
        {
            "id": "4ee65d06-279d-4f2b-a492-6185c8a59dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c6318b-5d0d-46dd-bb40-17b91d1c1863",
            "compositeImage": {
                "id": "084f8224-d474-4d02-9dab-70cf0b100f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee65d06-279d-4f2b-a492-6185c8a59dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e693ef-230e-4d13-a0a6-05b1e0e36ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee65d06-279d-4f2b-a492-6185c8a59dbb",
                    "LayerId": "e4c23936-d715-4e77-9f40-7fbeda1c73a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e4c23936-d715-4e77-9f40-7fbeda1c73a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c6318b-5d0d-46dd-bb40-17b91d1c1863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}