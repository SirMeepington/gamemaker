{
    "id": "9b3bfd7f-9ac4-43ff-a06b-c3282fa3cd3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_road",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d285725c-4337-4a2b-bda9-d7dd936f2e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b3bfd7f-9ac4-43ff-a06b-c3282fa3cd3c",
            "compositeImage": {
                "id": "bf685dee-2d3c-4594-827a-1e58d29d145b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d285725c-4337-4a2b-bda9-d7dd936f2e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a700a4f8-4dfe-4ee1-a511-fb8575f1905b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d285725c-4337-4a2b-bda9-d7dd936f2e3d",
                    "LayerId": "4cff2d37-a58a-43fe-af46-a2e09da28a32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "4cff2d37-a58a-43fe-af46-a2e09da28a32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b3bfd7f-9ac4-43ff-a06b-c3282fa3cd3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}