{
    "id": "ffd3755c-e44a-4a4f-b8fc-05985f665a09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 2,
    "bbox_right": 510,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1b0dc785-9cee-45aa-bccc-214d702d44b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffd3755c-e44a-4a4f-b8fc-05985f665a09",
            "compositeImage": {
                "id": "d0e48cad-e72a-4cfd-bcff-75fff4816675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0dc785-9cee-45aa-bccc-214d702d44b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20fcde3a-20d1-47f9-a218-7008a6c8647b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0dc785-9cee-45aa-bccc-214d702d44b3",
                    "LayerId": "540aa7a3-914c-449b-a36b-ba0e7da8e762"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "540aa7a3-914c-449b-a36b-ba0e7da8e762",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffd3755c-e44a-4a4f-b8fc-05985f665a09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}