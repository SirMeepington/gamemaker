{
    "id": "b6e58067-67cf-4ce1-a2d9-f48d16a91f7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "db0a084c-1269-48c1-bec9-db8b14b689e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e58067-67cf-4ce1-a2d9-f48d16a91f7b",
            "compositeImage": {
                "id": "3e552099-9d5c-4fd6-9653-84637a7a348e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0a084c-1269-48c1-bec9-db8b14b689e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "327126da-71ca-4e8f-90a5-e4f8bdbffcb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0a084c-1269-48c1-bec9-db8b14b689e4",
                    "LayerId": "01f334fd-dfa4-4b5f-a7f1-e318b4b8b6b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "01f334fd-dfa4-4b5f-a7f1-e318b4b8b6b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6e58067-67cf-4ce1-a2d9-f48d16a91f7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}