{
    "id": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exitbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0454147b-80c6-4629-bc6c-de9349720f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
            "compositeImage": {
                "id": "80592945-d0b9-4d76-85fd-feb96eb8429e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0454147b-80c6-4629-bc6c-de9349720f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a5d514-5a8c-4275-8e74-07bfe6c52a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0454147b-80c6-4629-bc6c-de9349720f43",
                    "LayerId": "391a2c43-95c4-4409-acad-da70c0a479a2"
                }
            ]
        },
        {
            "id": "d6533c58-013d-4b22-bec9-34fd7fa9bab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
            "compositeImage": {
                "id": "6085f9e2-cbc1-4545-9d61-59fcf5c2b1e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6533c58-013d-4b22-bec9-34fd7fa9bab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f66be2-e139-441d-9152-4c3e1a01f736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6533c58-013d-4b22-bec9-34fd7fa9bab2",
                    "LayerId": "391a2c43-95c4-4409-acad-da70c0a479a2"
                }
            ]
        },
        {
            "id": "e598ece0-cc6d-4852-b91b-7d2b2816f077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
            "compositeImage": {
                "id": "aeac9e5a-fb6f-42bb-9a1b-8daddc2ebbf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e598ece0-cc6d-4852-b91b-7d2b2816f077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d925d20-1f65-4108-a0e6-66bb8a66ac9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e598ece0-cc6d-4852-b91b-7d2b2816f077",
                    "LayerId": "391a2c43-95c4-4409-acad-da70c0a479a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "391a2c43-95c4-4409-acad-da70c0a479a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 34
}