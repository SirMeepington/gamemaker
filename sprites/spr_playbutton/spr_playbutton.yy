{
    "id": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3a2b11d3-c3d1-4892-a768-4ce174f103d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
            "compositeImage": {
                "id": "977e4169-09d4-4c77-8269-89b24124a4c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2b11d3-c3d1-4892-a768-4ce174f103d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "480cfcfc-a87a-419e-b1b0-3b2a9b790a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2b11d3-c3d1-4892-a768-4ce174f103d8",
                    "LayerId": "65055fbd-bcc4-4ac1-b9af-3c8a278885d8"
                }
            ]
        },
        {
            "id": "a04c938a-c5bf-4409-a564-6bada4a2d0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
            "compositeImage": {
                "id": "e3549bd0-fe82-498a-aa2b-85678f1c211f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04c938a-c5bf-4409-a564-6bada4a2d0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d20d41-07e4-4e0f-98b2-c0a33043489a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04c938a-c5bf-4409-a564-6bada4a2d0cd",
                    "LayerId": "65055fbd-bcc4-4ac1-b9af-3c8a278885d8"
                }
            ]
        },
        {
            "id": "8818593a-6d45-4ee1-b729-0e6c2276739e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
            "compositeImage": {
                "id": "192b836d-1b42-4d42-a137-67f2b4ce2a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8818593a-6d45-4ee1-b729-0e6c2276739e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d91b70ba-08ee-4ce3-8689-755c07485828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8818593a-6d45-4ee1-b729-0e6c2276739e",
                    "LayerId": "65055fbd-bcc4-4ac1-b9af-3c8a278885d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "65055fbd-bcc4-4ac1-b9af-3c8a278885d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 34
}