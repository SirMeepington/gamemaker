{
    "id": "409568a3-a55f-45a4-b05a-46b67f3c5d89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 14,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 88,
    "frames": [
        {
            "id": "15d7befc-4d05-4cee-bdfa-9ef551ae5d92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409568a3-a55f-45a4-b05a-46b67f3c5d89",
            "compositeImage": {
                "id": "9ff4d06e-cf44-4dd4-992b-f6decd7929e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15d7befc-4d05-4cee-bdfa-9ef551ae5d92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14e17c60-e0bd-49fa-b63b-afa2e80a6a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15d7befc-4d05-4cee-bdfa-9ef551ae5d92",
                    "LayerId": "fbc4239f-f9fd-42b7-a6eb-4eb922ee2611"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbc4239f-f9fd-42b7-a6eb-4eb922ee2611",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409568a3-a55f-45a4-b05a-46b67f3c5d89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}