{
    "id": "a94227fd-306a-4f95-a4c5-0c0f547342f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_road_barrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12678f9a-c52c-4aae-88e3-91c20760b629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94227fd-306a-4f95-a4c5-0c0f547342f4",
            "compositeImage": {
                "id": "1689bf61-b33d-455c-ad45-442cae82108f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12678f9a-c52c-4aae-88e3-91c20760b629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfbf4c3-b272-4435-903f-73a66a2197cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12678f9a-c52c-4aae-88e3-91c20760b629",
                    "LayerId": "e1278005-f0d8-4a6d-a0aa-0f6c97de2351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "e1278005-f0d8-4a6d-a0aa-0f6c97de2351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a94227fd-306a-4f95-a4c5-0c0f547342f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}