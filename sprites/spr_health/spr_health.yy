{
    "id": "1a055c43-5c64-48b6-b6e0-afa57cc3f207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1dec328c-ab3a-414a-b4f8-2d6b073d71ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a055c43-5c64-48b6-b6e0-afa57cc3f207",
            "compositeImage": {
                "id": "bcef325a-581b-441e-be45-1adbf07f0716",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dec328c-ab3a-414a-b4f8-2d6b073d71ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9919f830-beef-4edb-a443-1f7bb2e8d917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dec328c-ab3a-414a-b4f8-2d6b073d71ef",
                    "LayerId": "b9ebf889-9588-44c8-83d5-343fdddaeb3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b9ebf889-9588-44c8-83d5-343fdddaeb3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a055c43-5c64-48b6-b6e0-afa57cc3f207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}