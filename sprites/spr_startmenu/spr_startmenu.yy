{
    "id": "9f2add6b-7bc3-457a-bbc8-58c0d292a638",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c2bfbaed-d4e5-421a-a618-ff467e886581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f2add6b-7bc3-457a-bbc8-58c0d292a638",
            "compositeImage": {
                "id": "a8862c27-40f3-4877-81ab-0687f1d5ad49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bfbaed-d4e5-421a-a618-ff467e886581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f345c974-96a9-451b-8d1b-6097cda3b437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bfbaed-d4e5-421a-a618-ff467e886581",
                    "LayerId": "558d2c22-1bf8-4524-bea0-fbdc789d2754"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "558d2c22-1bf8-4524-bea0-fbdc789d2754",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f2add6b-7bc3-457a-bbc8-58c0d292a638",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}