{
    "id": "ca999d88-7f2a-4bc3-8c30-281344c1e15c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_cyan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 19,
    "bbox_right": 104,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 75,
    "frames": [
        {
            "id": "eb30e27f-280b-4240-a55e-0a412fc95c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca999d88-7f2a-4bc3-8c30-281344c1e15c",
            "compositeImage": {
                "id": "4b7a3db5-3af2-493b-99e8-f2ae9008977c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb30e27f-280b-4240-a55e-0a412fc95c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eeef1a3-106a-438b-b453-d1945cb31373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb30e27f-280b-4240-a55e-0a412fc95c5a",
                    "LayerId": "ce472df6-b7be-455c-86a9-3222799f25b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ce472df6-b7be-455c-86a9-3222799f25b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca999d88-7f2a-4bc3-8c30-281344c1e15c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}