{
    "id": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_swat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 2,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "167948e0-f30a-4ee3-815f-3914d280e62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
            "compositeImage": {
                "id": "d1676783-56e8-4baf-9063-388ceb868a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167948e0-f30a-4ee3-815f-3914d280e62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b565d284-a178-428a-8015-425191e1d6a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167948e0-f30a-4ee3-815f-3914d280e62b",
                    "LayerId": "25b1c41d-2f1d-4e7d-a39c-d96e61410f93"
                }
            ]
        },
        {
            "id": "436ccbd9-d78c-4cfb-907a-194112fd48d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
            "compositeImage": {
                "id": "2bf43219-1c56-410f-8640-34fd68727f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "436ccbd9-d78c-4cfb-907a-194112fd48d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c6aee8-346d-41dc-91a8-f9e72ee3a648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "436ccbd9-d78c-4cfb-907a-194112fd48d4",
                    "LayerId": "25b1c41d-2f1d-4e7d-a39c-d96e61410f93"
                }
            ]
        },
        {
            "id": "a5c033db-b76d-48bb-be7e-836bee033637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
            "compositeImage": {
                "id": "dfea4043-ef7b-4d0b-acd8-a73e1ce6fcbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c033db-b76d-48bb-be7e-836bee033637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c29c971-b537-42a6-a9aa-715cb9350e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c033db-b76d-48bb-be7e-836bee033637",
                    "LayerId": "25b1c41d-2f1d-4e7d-a39c-d96e61410f93"
                }
            ]
        },
        {
            "id": "81cf0601-24c4-4323-afd1-5b1a78b9680a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
            "compositeImage": {
                "id": "d9d4c88b-6385-4f03-b442-64bcd5df4ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81cf0601-24c4-4323-afd1-5b1a78b9680a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25430b86-1510-4cba-a259-b3275ef5b67b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81cf0601-24c4-4323-afd1-5b1a78b9680a",
                    "LayerId": "25b1c41d-2f1d-4e7d-a39c-d96e61410f93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "25b1c41d-2f1d-4e7d-a39c-d96e61410f93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}