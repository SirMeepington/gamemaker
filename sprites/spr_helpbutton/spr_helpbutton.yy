{
    "id": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_helpbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "45c78b86-fe67-4de7-a221-ae182c78ec0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
            "compositeImage": {
                "id": "351ea697-d589-4526-8451-0b88e3f317c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c78b86-fe67-4de7-a221-ae182c78ec0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40a8800-fb74-4d5e-8931-b2313a0d4722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c78b86-fe67-4de7-a221-ae182c78ec0e",
                    "LayerId": "7d8bbcb5-1080-4965-84bd-5c1df71158a7"
                }
            ]
        },
        {
            "id": "19cbcb92-5f17-4bf8-b9a5-0ce717d3050a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
            "compositeImage": {
                "id": "ca313bb6-4c22-4e8a-8eb1-4a2f6d7fbc0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19cbcb92-5f17-4bf8-b9a5-0ce717d3050a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32efc121-4c37-4a12-bc2a-d2e4bc841b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19cbcb92-5f17-4bf8-b9a5-0ce717d3050a",
                    "LayerId": "7d8bbcb5-1080-4965-84bd-5c1df71158a7"
                }
            ]
        },
        {
            "id": "88327523-9993-4386-9f10-a1ab832418ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
            "compositeImage": {
                "id": "07ffdf8c-90df-4ebc-9c2d-595e2cf5adb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88327523-9993-4386-9f10-a1ab832418ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3235464-ed7a-420d-929e-05884ea52f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88327523-9993-4386-9f10-a1ab832418ba",
                    "LayerId": "7d8bbcb5-1080-4965-84bd-5c1df71158a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 71,
    "layers": [
        {
            "id": "7d8bbcb5-1080-4965-84bd-5c1df71158a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 35
}