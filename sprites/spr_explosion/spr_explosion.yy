{
    "id": "8927761e-c107-4d84-a545-eb980e9080f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 24,
    "bbox_right": 102,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0ea85e23-adf0-4b63-80ec-555a4a3d77a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
            "compositeImage": {
                "id": "b8a77cd3-fbb6-454b-9efd-d583dedebaf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea85e23-adf0-4b63-80ec-555a4a3d77a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2aac94-99ab-44df-a96d-10bdb8d54a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea85e23-adf0-4b63-80ec-555a4a3d77a5",
                    "LayerId": "88ede43d-7d11-4c92-bc29-c3d9320825d3"
                }
            ]
        },
        {
            "id": "ee32720e-3bdb-4aa7-b430-f275e6d3a758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
            "compositeImage": {
                "id": "c220ecc1-8155-4871-adba-8699a97dca5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee32720e-3bdb-4aa7-b430-f275e6d3a758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b55da03-c4f7-40e7-b3e9-3043c1589137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee32720e-3bdb-4aa7-b430-f275e6d3a758",
                    "LayerId": "88ede43d-7d11-4c92-bc29-c3d9320825d3"
                }
            ]
        },
        {
            "id": "55b9dc5c-03dd-4e32-9ef5-6ebd80b1b33d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
            "compositeImage": {
                "id": "62a45756-98fa-466d-bc6c-ff58f3435594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b9dc5c-03dd-4e32-9ef5-6ebd80b1b33d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e141c71-ab2d-46eb-b507-67562397dba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b9dc5c-03dd-4e32-9ef5-6ebd80b1b33d",
                    "LayerId": "88ede43d-7d11-4c92-bc29-c3d9320825d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "88ede43d-7d11-4c92-bc29-c3d9320825d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}