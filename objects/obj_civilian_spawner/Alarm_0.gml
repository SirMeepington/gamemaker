/// @description Spawn

if (canspawn){
	laneno = round(random_range(0,5));
	lane = obj_game_manager.lanes[laneno];

	if (obj_game_manager.inuse[laneno] == 1){
		alarm[0] = 1;
	} else {
		var civ = instance_create_layer(lane,1300,"Enemies",obj_civilian);
		obj_game_manager.inuse[laneno] = 1;
		//show_debug_message("Spawned civilian");
		alarm[0] = 240;
	}
} else {
	alarm[0] = 240;
}