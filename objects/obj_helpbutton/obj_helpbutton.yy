{
    "id": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_helpbutton",
    "eventList": [
        {
            "id": "5938ad10-6b47-4a75-a395-ca76bc2ce233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e"
        },
        {
            "id": "2202e274-d209-458e-9731-9f8f55427f9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e"
        },
        {
            "id": "a4df245e-9245-413f-b272-68d191bce711",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e"
        },
        {
            "id": "989fddd7-9261-4bbf-9c0a-6843d8bdb545",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e"
        },
        {
            "id": "a3961d06-120e-47a5-aeac-32ce998547fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "4b51f1ce-3a99-4f44-b7d0-f7d89b034b3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f003ae37-3c15-4279-9ffa-fee2c8e69881",
    "visible": true
}