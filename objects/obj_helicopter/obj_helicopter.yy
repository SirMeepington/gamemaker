{
    "id": "c6baf7e1-53fd-4c38-8ad7-60b772b0f41d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_helicopter",
    "eventList": [
        {
            "id": "cad6650a-a81e-44b1-93d2-2876d02bfc58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6baf7e1-53fd-4c38-8ad7-60b772b0f41d"
        },
        {
            "id": "d0ab6d0d-dd8f-4a02-acf1-69b496c43706",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6baf7e1-53fd-4c38-8ad7-60b772b0f41d"
        },
        {
            "id": "28319f9c-ace9-42cc-9c4b-0329aa5922cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c6baf7e1-53fd-4c38-8ad7-60b772b0f41d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "023fd6c0-498b-493c-b3d3-2df28ac140c6",
    "visible": true
}