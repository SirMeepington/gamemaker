if (room == rm_gameover){
	var survivalmins = survivaltime div 60;
	var survivalsecs = survivaltime % 60;

	pscore = round(survivaltime/2)+(enemykills*5)+(bosskills*20);

	draw_set_color(c_white)
	draw_set_font(fnt_menu);
	draw_set_halign(fa_center);

	if (survivalmins > 0){
		draw_text(320,450,"Survived for: "+string(survivalmins)+"m "+string(survivalsecs)+"s")
	} else {
		draw_text(320,450,"Survived for: "+string(survivalsecs)+"s")
	}

	draw_set_color(c_white)
	draw_set_font(fnt_menu);
	draw_set_halign(fa_center);	
	draw_text(320,500,"Enemies killed: "+string(enemykills));

	draw_set_color(c_white)
	draw_set_font(fnt_menu);
	draw_set_halign(fa_center);
	draw_text(320,550,"Boss Kills: "+string(bosskills));

	draw_set_color(c_white)
	draw_set_font(fnt_menu);
	draw_set_halign(fa_center);
	draw_text(320,600,"Total Score: "+string(pscore));
}

if (room == rm_menu){
	instance_destroy();
}