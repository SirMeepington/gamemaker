{
    "id": "beceef79-07e9-470a-ae0d-dd1dd601d6a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powerup",
    "eventList": [
        {
            "id": "25f18f2b-27d6-470f-8bfb-209c38f08188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "beceef79-07e9-470a-ae0d-dd1dd601d6a8"
        },
        {
            "id": "5f15a9a2-7eea-44d2-ba71-c0887d7580d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "beceef79-07e9-470a-ae0d-dd1dd601d6a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b164bece-2b8a-48dd-bf7c-685d54bbf1f8",
    "visible": true
}