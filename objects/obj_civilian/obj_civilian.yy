{
    "id": "dfa43643-6b15-4b8a-a744-76c79cdbfb81",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_civilian",
    "eventList": [
        {
            "id": "4d840a76-2911-43da-ad9d-283f3d62592c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dfa43643-6b15-4b8a-a744-76c79cdbfb81"
        },
        {
            "id": "b7a1da8c-20f9-4251-969e-8569855f4fb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dfa43643-6b15-4b8a-a744-76c79cdbfb81"
        },
        {
            "id": "59d9f2e8-5697-4511-93e1-ba4683eea893",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dfa43643-6b15-4b8a-a744-76c79cdbfb81"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "3c8de1b4-2856-4294-b5dc-02e09fc7d119",
    "visible": true
}