{
    "id": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exitbutton",
    "eventList": [
        {
            "id": "aa477dfb-547b-4b2a-9522-3d3384582b13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d"
        },
        {
            "id": "371b601a-0006-4e5f-9d91-d094b95597c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d"
        },
        {
            "id": "cd6372b3-d99d-46a2-a49b-cd604274e2d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d"
        },
        {
            "id": "4f7975ea-fa8d-4dec-b535-6f4c62788f0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d"
        },
        {
            "id": "4dc199b1-4159-443c-899e-32ebd080435a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "0ef5bbb3-5464-4c47-865f-3bfbcef8a55d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8892b1c6-4a26-407d-bd4e-6838f6db39eb",
    "visible": true
}