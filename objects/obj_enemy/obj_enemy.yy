{
    "id": "ec8e5726-7908-4f58-be72-81131fdab17e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "68074322-d30e-44e0-ae84-0a5df25da87a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        },
        {
            "id": "1cdd7769-5cb4-46cb-87c3-cd4e9d3ecfdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        },
        {
            "id": "29173cb3-b194-4707-a1ba-79dc1165845a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        },
        {
            "id": "2f677ca9-10ed-42f2-9774-147cf1e7b371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        },
        {
            "id": "7ef9c5c2-2d1c-41bb-8d2c-19328dab2c18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d471e04-55b1-45f5-8ad5-1cddfb73bf46",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        },
        {
            "id": "2face944-7c0b-4274-b17a-d23151fafbe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ec8e5726-7908-4f58-be72-81131fdab17e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "56ef9467-0c80-401c-874e-94df89b92fb2",
    "visible": true
}