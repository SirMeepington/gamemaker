/// @description Prepare spawn

show_debug_message("Alarm tick")
if (!isspawned){
	show_debug_message("Clearing screen");

	obj_civilian_spawner.canspawn = false;
	obj_bike_spawner.canspawn = false;
	obj_car_spawner.canspawn = false;
	
	alarm[1] = 180;
}