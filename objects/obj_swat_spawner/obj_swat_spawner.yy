{
    "id": "2182542d-b6f4-4ff8-9923-ef1173ed1d07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_swat_spawner",
    "eventList": [
        {
            "id": "06b64db1-8b46-4a76-8694-d63bd4810dec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2182542d-b6f4-4ff8-9923-ef1173ed1d07"
        },
        {
            "id": "ca35c6b1-853a-49b4-8070-22872924aed7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2182542d-b6f4-4ff8-9923-ef1173ed1d07"
        },
        {
            "id": "a96a69ac-6532-4804-a9d6-f17a5605b213",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2182542d-b6f4-4ff8-9923-ef1173ed1d07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}