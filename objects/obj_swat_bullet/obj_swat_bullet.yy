{
    "id": "e6923367-bd11-4518-adbf-54020f865634",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_swat_bullet",
    "eventList": [
        {
            "id": "cafcd7d7-4da0-4a1c-a001-0bde9f7b9b78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e6923367-bd11-4518-adbf-54020f865634"
        },
        {
            "id": "d60ee4f3-a17e-46d0-b9da-0e4bb22d56a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e6923367-bd11-4518-adbf-54020f865634"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "409568a3-a55f-45a4-b05a-46b67f3c5d89",
    "visible": true
}