{
    "id": "45b4616a-3530-4e7b-9c1c-95562f215b70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_health_pack",
    "eventList": [
        {
            "id": "29efcedc-1882-4f38-9498-991177f1f57d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "45b4616a-3530-4e7b-9c1c-95562f215b70"
        },
        {
            "id": "1f3ebf7d-5d71-4f0f-9710-568eb9a79f37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45b4616a-3530-4e7b-9c1c-95562f215b70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1a055c43-5c64-48b6-b6e0-afa57cc3f207",
    "visible": true
}