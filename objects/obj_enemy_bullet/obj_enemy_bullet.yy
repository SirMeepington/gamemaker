{
    "id": "60b13ce7-a4b1-4753-a0ee-cae99b5d12ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_bullet",
    "eventList": [
        {
            "id": "a1480a28-60d5-48e9-8d5a-39442000331a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60b13ce7-a4b1-4753-a0ee-cae99b5d12ab"
        },
        {
            "id": "789a404f-03ce-441b-bf09-126fa7d81abb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60b13ce7-a4b1-4753-a0ee-cae99b5d12ab"
        },
        {
            "id": "da28dcbb-7008-4731-aaef-c4eb83dc18a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60b13ce7-a4b1-4753-a0ee-cae99b5d12ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "409568a3-a55f-45a4-b05a-46b67f3c5d89",
    "visible": true
}