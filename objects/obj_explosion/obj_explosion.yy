{
    "id": "ec0f43bc-aae3-45a8-b1fc-be9a8ca4ee76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_explosion",
    "eventList": [
        {
            "id": "4a493dd3-5026-4611-a294-0914cc56c04c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ec0f43bc-aae3-45a8-b1fc-be9a8ca4ee76"
        },
        {
            "id": "160a7d09-3761-4692-a766-68b2b194a24d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec0f43bc-aae3-45a8-b1fc-be9a8ca4ee76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
    "visible": true
}