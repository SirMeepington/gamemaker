{
    "id": "1946cf3a-536b-43b6-aae4-92e7e5b8e228",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_helicopter_spawner",
    "eventList": [
        {
            "id": "d8fd91db-46c4-4fd8-92b6-b4203d6b0c30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1946cf3a-536b-43b6-aae4-92e7e5b8e228"
        },
        {
            "id": "2cd91cc6-b30d-40ec-a076-3b7c60e35396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1946cf3a-536b-43b6-aae4-92e7e5b8e228"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}