{
    "id": "d735653e-374a-4a5c-97ca-9f1008722a4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_retrybutton",
    "eventList": [
        {
            "id": "82cd0310-e323-460d-b90d-6d22c253e704",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d735653e-374a-4a5c-97ca-9f1008722a4d"
        },
        {
            "id": "e39afea2-1007-4a31-b053-18d07b129720",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "d735653e-374a-4a5c-97ca-9f1008722a4d"
        },
        {
            "id": "920469d9-e663-4b81-9fe8-444f89551b36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d735653e-374a-4a5c-97ca-9f1008722a4d"
        },
        {
            "id": "70df8126-9ee7-49f3-88de-1f8ce5c6f76e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "d735653e-374a-4a5c-97ca-9f1008722a4d"
        },
        {
            "id": "0230eed2-aede-4f39-b68f-d8281c43ebb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "d735653e-374a-4a5c-97ca-9f1008722a4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "75273d05-5ee0-4d29-a3fb-e9175f58d5ea",
    "visible": true
}