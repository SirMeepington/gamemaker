/// @description Easier game closing

if (keyboard_check_released(vk_escape)){
//	show_debug_message(room)
	if (room == rm_menu){
		game_end();
	} else {
		room_goto(rm_menu);
	}
}
if (room == rm_menu){
	inuse[0] = 0;
	inuse[1] = 0;
	inuse[2] = 0;
	inuse[3] = 0;
	inuse[4] = 0;
	inuse[5] = 0;
}