/// @description Global game variables

inuse[0] = 0;
inuse[1] = 0;
inuse[2] = 0;
inuse[3] = 0;
inuse[4] = 0;
inuse[5] = 0;


// Lane Co-ords
lanes[0] = 64;
lanes[1] = 160;
lanes[2] = 256;

lanes[3] = 384;
lanes[4] = 480;
lanes[5] = 576;


audio_play_sound(snd_music,1,true);
