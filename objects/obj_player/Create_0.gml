/// @description Runs player instantiation
/*
// Lane coords
lane = 0;
lanes[0] = 64;
lanes[1] = 160;
lanes[2] = 256;

lanes[3] = 384;
lanes[4] = 480;
lanes[5] = 576;
*/

alive = true;

// Vars
maxhp = 100;
hp = 100;
rotation_limit = 10;
speed_limit = 10;
rotation_speed = 1;/**/
damage = 1;
can_shoot = true;

survivaltime = 0;
survivaltick = 0;
