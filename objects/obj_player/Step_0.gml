/// @description Movements
y = 768;
if (alive){
	if (hp <= 0){
		alive = false;
		instance_create_layer(x,y,"Explosions",obj_explosion);
		alarm[1] = 60;
		return;
	}

	if (keyboard_check(ord("D")) && keyboard_check(ord("A"))){
		x = x;
		if (image_angle < 0){
			image_angle++;
		} else if (image_angle > 0){
			image_angle--;
		}
	} else if (keyboard_check(ord("D"))){
		if (x<576){
			x+=speed_limit;
			if (image_angle<=rotation_limit){
				image_angle+=rotation_speed;
			}
		}
	} else if (keyboard_check(ord("A"))){
		if (x>64){
			x-=speed_limit;
			if(image_angle>=-rotation_limit){
				image_angle-=rotation_speed;
			}

		}
	} else {
		if (image_angle < 0){
			image_angle++;
		} else if (image_angle > 0){
			image_angle--;
		}
	}

	if (can_shoot){
		if (keyboard_check(vk_up)){
			speed_limit = 2;
			can_shoot = false;
			var bullet = instance_create_layer(x,y,"Bullets",obj_bullet);
			with (bullet){
				dir = 0
			}
			alarm[0]=10; 
		} else if (keyboard_check(vk_left)){
			speed_limit = 2;
			can_shoot = false;
			var bullet = instance_create_layer(x,y,"Bullets",obj_bullet);
			with (bullet){
				dir = -90
			}
			alarm[0]=10; 
		} else if (keyboard_check(vk_right)){
			speed_limit = 2;
			can_shoot = false;
			var bullet = instance_create_layer(x,y,"Bullets",obj_bullet);
			with (bullet){
				dir = 90
			}
			alarm[0]=10; 
		} else {
			speed_limit = 10;
		}
	}


	survivaltick++;
	if (survivaltick >= room_speed){
		survivaltime+=1;
		obj_player_stats.survivaltime = survivaltime;
		survivaltick = 0;
	}
}