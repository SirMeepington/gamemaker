if (alive)
{
	draw_self()

	draw_set_color(c_red);
	draw_rectangle(x-64,y-96,x+64,y-80,false);

	draw_set_color(c_lime);
	draw_rectangle(x-64,y-96,(x-64)+(128/(maxhp/hp)),y-80,false);

	draw_set_color(c_white);
	draw_set_font(fnt_timer);
	draw_set_halign(fa_middle);

	var survivalmins = survivaltime div 60;
	var survivalsecs = survivaltime % 60;
	if (survivalmins > 0){
		draw_text(x,y+96,"Survived for: "+string(survivalmins)+"m "+string(survivalsecs)+"s")
	} else {
		draw_text(x,y+96,"Survived for: "+string(survivalsecs)+"s")
	}
	
//	draw_text(x,y+120,"Damage: "+string(damage));
}