{
    "id": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "8490c506-dc93-436d-8c8d-f4fd90252b9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d"
        },
        {
            "id": "30cf18b9-84f2-4f51-802f-42d12628b9d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d"
        },
        {
            "id": "ea6418fb-06fc-476e-b315-b118442dc94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d"
        },
        {
            "id": "d56966b1-467c-4bc7-84f8-03630cc8f982",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d"
        },
        {
            "id": "aae0dfe3-cf34-4bc5-969e-82e3d3bf5db4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7affd203-f3aa-40f3-983a-d9e25ced224d",
    "visible": true
}