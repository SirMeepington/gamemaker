/// @description Spawn bullets

if (ready && shooting){ 
	instance_create_layer(x - ((sprite_width / 5)*sin(random_range(0.5,1.5)*delta_time)), y + (sprite_width/10),"Bullets",obj_swat_bullet) // left
	instance_create_layer(x + ((sprite_width / 5)*sin(random_range(0.5,1.5)*delta_time)), y + (sprite_width/10),"Bullets",obj_swat_bullet) // left
	alarm[2] = firerate;
}