{
    "id": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_swat",
    "eventList": [
        {
            "id": "b0d511dc-142c-440c-8fe7-a5e01056db02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "d2120795-c172-4a52-baeb-866cef494c46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "e2ada2a1-c83d-4b4a-9d27-00bf128bc455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "7fd3410c-719e-4a0f-8b1e-fd6d076f7da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "da40a34a-954f-48f0-952e-33245ffff3f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d471e04-55b1-45f5-8ad5-1cddfb73bf46",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "731a276b-7e6c-4abe-9688-18c4dc1fb306",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        },
        {
            "id": "9fd64456-2023-4a68-a43f-1af8138289c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e9bddd9f-11eb-41d2-b8a9-3f40d83dd4fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a99da7b4-2a9b-4258-a9e2-1b898c7f591f",
    "visible": true
}