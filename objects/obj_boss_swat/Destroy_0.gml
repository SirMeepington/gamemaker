obj_swat_spawner.isspawned = false;
obj_bike_spawner.canspawn = true;
obj_car_spawner.canspawn = true;
obj_civilian_spawner.canspawn = true;
obj_swat_spawner.alarm[0] = obj_swat_spawner.spawndelay;
obj_player_stats.bosskills++;


// So the drops dont spawn inside each other
var xmin = x-64;
if (xmin < 64) xmin = 64;
var xmax = x+64;
if (xmax > 576) xmax = 576;

// Always spawn health pack
instance_create_layer(random_range(xmax,xmin),y,"Pickups",obj_health_pack);

// Sometimes spawn damage pack
instance_create_layer(random_range(xmax,xmin),y,"Pickups",obj_powerup);
