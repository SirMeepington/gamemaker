/// @description Spawn

if (canspawn){
	laneno = round(random_range(0,5));
	lane = obj_game_manager.lanes[laneno];

	if (obj_game_manager.inuse[laneno] == 1){
		alarm[0] = 1;
	} else {
	show_debug_message("Bike spawned on lane "+string(lane)+" ("+string(laneno)+")");

	var bike = instance_create_layer(lane,0,"Enemies",obj_bike_police);
	obj_game_manager.inuse[laneno] = 1;
	alarm[0] = 180;

	}
} else {
	alarm[0] = 180;
}