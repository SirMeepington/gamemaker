/// @description Hit by player

// check lane
var lane;
for (i=0; i<array_length_1d(obj_game_manager.lanes);i++){
	if (obj_game_manager.lanes[i] == x){
		lane = i;
	}
}

obj_game_manager.inuse[lane] = 0;
obj_player_stats.enemykills++;

with(other){
	hp-=5;
}

instance_change(obj_explosion, true);
