{
    "id": "ce0106d3-cf10-4b1f-8196-c40d9f982a82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bike_police",
    "eventList": [
        {
            "id": "c26bd77c-4d47-4172-ae25-1955efac0962",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        },
        {
            "id": "0ce0762a-9f00-4c75-9105-d63554d1b610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        },
        {
            "id": "7622840d-86e9-4b7b-845f-3bebf6e5992e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        },
        {
            "id": "086f7327-d119-4e8f-bab3-1ecbfc9806d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        },
        {
            "id": "f01d8a2b-f0fe-429f-ab36-0318c663b669",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d471e04-55b1-45f5-8ad5-1cddfb73bf46",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        },
        {
            "id": "e6c3ccab-bfa8-48d8-b6bd-70632031efc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ce0106d3-cf10-4b1f-8196-c40d9f982a82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "d3c6318b-5d0d-46dd-bb40-17b91d1c1863",
    "visible": true
}