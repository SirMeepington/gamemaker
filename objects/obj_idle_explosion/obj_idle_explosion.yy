{
    "id": "65f4ae08-2b06-4869-a9c5-f261beacd43c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_idle_explosion",
    "eventList": [
        {
            "id": "12937072-ec0c-43e5-9980-9f9ae605a9a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "65f4ae08-2b06-4869-a9c5-f261beacd43c"
        },
        {
            "id": "201cc7ea-cfc7-4ddd-89ee-d7f45778f863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a984ee9e-3c7e-4d37-a5ad-cce0941f4d1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "65f4ae08-2b06-4869-a9c5-f261beacd43c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8927761e-c107-4d84-a545-eb980e9080f2",
    "visible": true
}