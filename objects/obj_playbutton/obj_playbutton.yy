{
    "id": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playbutton",
    "eventList": [
        {
            "id": "ab81affb-eefc-4176-b9d4-ee08aa5aaea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8"
        },
        {
            "id": "0662a70d-0cac-4047-a66a-c3cdea641312",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8"
        },
        {
            "id": "3e0863cb-b208-4b93-b474-fd9f1abe9cb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8"
        },
        {
            "id": "28ce3741-96b7-433b-b77d-564411f50c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8"
        },
        {
            "id": "8798735a-94be-4638-b8b0-3e00af4341ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "de6bf790-5ea7-4b3c-b9a0-451d44204eb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4765da9f-607c-4648-8591-cd7cdc2e3e65",
    "visible": true
}