/// @description Spawn

if (canspawn && obj_player.survivaltime > 30){
	laneno = round(random_range(0,5));
	lane = obj_game_manager.lanes[laneno];

	if (obj_game_manager.inuse[laneno] == 1){
		alarm[0] = 1;
	} else {
	show_debug_message("Car spawned on lane "+string(lane)+" ("+string(laneno)+")");

	var car = instance_create_layer(lane,0,"Enemies",obj_enemy);
	obj_game_manager.inuse[laneno] = 1;
	alarm[0] = 360;

	}
} else {
	alarm[0] = 360;
}