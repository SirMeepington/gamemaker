{
    "id": "9d654abb-38b3-46c0-ad2d-3ac1c75b27a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_car_spawner",
    "eventList": [
        {
            "id": "31e52ed1-796b-4d5a-87ba-1b1c4268e466",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d654abb-38b3-46c0-ad2d-3ac1c75b27a0"
        },
        {
            "id": "e1a20cb6-f758-4391-8210-4e379bd89937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9d654abb-38b3-46c0-ad2d-3ac1c75b27a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}